# Flask Example Application for practicing deployments

- initial flask app:
  - designed to be deployed with WSGI interface (wsgi, gunicorn)
  - can also be managed with application manage tools (supervisord)
  - to setup application:
    - install `pipenv` package either with pip or your systems package manager (apt,yum,pacman,brew)
    - cd to folder named `app`
    - run `pipenv install`, to install depencencies of the project
    - once command above is done, run `pipenv shell`
    - after new shell is opened, use `flask run` to run the application
    - to run the app as service, please implement systemd service and use Exec command with next options:
      - `pipenv run gunicorn -w 4 app:app`
      - one might need path to application path for it to work
      - in order not to fail, please use webserver(nginx,apache,caddy)
